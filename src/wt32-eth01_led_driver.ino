// to enable DMA for the leds
#define FASTLED_ESP32_I2S true
#include <FastLED.h>

#include <ESParam.h>
#include <ESParamPrinter.h>
#include <ArtNet.h>
#include <Shadel.h>
#include <AnimUtils.h>

// LED output pins
#define LED_DATA_1_PIN 2
#define LED_DATA_2_PIN 4
#define LED_DATA_3_PIN 12
#define LED_DATA_4_PIN 14
#define LED_DATA_5_PIN 15
#define LED_DATA_6_PIN 17
#define LED_DATA_7_PIN 5
#define LED_DATA_8_PIN 33
// other pins
#define STATUS_LED_PIN 32
#define INPUT_A_PIN    36
#define INPUT_B_PIN    39
// led config
#define LED_TYPE       WS2813
#define COLOR_ORDER    GRB
#define NUM_STRIPS     8
#define NUM_LEDS_PER_STRIP 200
#define NUM_LEDS NUM_LEDS_PER_STRIP * NUM_STRIPS
CRGB leds[NUM_LEDS];

Pixel pixels[NUM_LEDS];
#define SHADER_STRIPS_COUNT 24
PixelStrip strips[SHADER_STRIPS_COUNT];

// strip IDX
#define CHAND_A 0
#define CHAND_B 1
#define CHAND_C 2
#define CHAND_D 3

#define TARP_EDGE_COUNT 4
#define TARP_EDGE 4
// #define TARP_EDGE 4
// #define TARP_EDGE 5
// #define TARP_EDGE 6
// #define TARP_EDGE 7
// #define TARP_EDGE 8
// #define TARP_EDGE 9

#define DRIP_A 8
#define DRIP_B 9

#define VASE_A 10
#define VASE_B 11
#define VASE_C 12

#define FLOODS_A 13
#define FLOODS_B 14
#define FLOODS_C 15

#define TREE_A 16
#define TREE_B 17 // actually the same tree
#define TREE_C 18
#define TREE_D 19
#define TREE_E 20

#define PANEL_A 21
#define PANEL_B 22

#define ANNEX_SIGN 23

int addresses[SHADER_STRIPS_COUNT][2] = {
  // chands     0-3
  {0, 12},
  {13, 31},
  {32, 49},
  {50, 81},
  // left edge  4-5
  {201, 248},
  {325, 361},
  // right edge 6-7
  {400, 433},
  {436, 482},
  // drips      8-9
  {356, 398},
  {501, 562},
  // vases      10-12
  {629, 640},
  {660, 671},
  {689, 700},
  // floods     13-15
  {1400, 1410},
  {1450, 1460},
  {1500, 1510},
  // trees      16-20
  {256, 285},
  {286, 317},
  {803, 996},
  {1069, 1193},
  {1220, 1396},
  // panels     21-22
  {745, 799},
  {1411, 1587},
  // annex sign 23
  {578, 597},
};

// params
ColorParam colorParam;
IntParam brightnessParam;
BoolParam enableAnimParam;
IntParam countParam;

IntParam hue1Param;
IntParam sat1Param;
IntParam val1Param;
IntParam hue2Param;
IntParam extentParam;
IntParam speedParam;
FloatParam posMultParam;
FloatParam incMultParam;
FloatParam sinMultParam;
FloatParam sinAddParam;
IntParam posAddParam;

Ramp dripRampA;
Ramp dripRampB;

void setup(){
  Serial.begin(115200);
  pinMode(STATUS_LED_PIN, OUTPUT);
  digitalWrite(STATUS_LED_PIN, HIGH);

  enableAnimParam.set("/anim/enable", true);
  enableAnimParam.saveType = SAVE_ON_REQUEST;
  paramCollector.add(&enableAnimParam);

  brightnessParam.set("/leds/bright", 0, 255, 200);
  brightnessParam.setCallback(updateBrightness);
  brightnessParam.saveType = SAVE_ON_REQUEST;
  paramCollector.add(&brightnessParam);

  /*
    colorParam.set("/anim/color", CRGB(255,0,0));
    colorParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&colorParam);
  */

    brightnessParam.set("/leds/bright", 0, 255, 200);
    brightnessParam.setCallback(updateBrightness);
    brightnessParam.saveType = SAVE_ON_REQUEST;

    hue1Param.set("/anim/hue1", 0, 255, 50);
    hue1Param.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&hue1Param);

    sat1Param.set("/anim/sat1", 0, 255, 50);
    sat1Param.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&sat1Param);

    val1Param.set("/anim/val1", 0, 255, 50);
    val1Param.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&val1Param);

    hue2Param.set("/anim/hue2", 0, 255, 150);
    hue2Param.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&hue2Param);

    posMultParam.set("/anim/posMult", 0, 10, 3.523);
    posMultParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&posMultParam);

    incMultParam.set("/anim/incMult", 0, 10, .1);
    incMultParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&incMultParam);

    sinMultParam.set("/anim/sinMult", 0, 1, .5);
    sinMultParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&sinMultParam);

    sinAddParam.set("/anim/sinAdd", 0, 1, .5);
    sinAddParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&sinAddParam);

    posAddParam.set("/anim/posAdd", 0, 10000, 100);
    posAddParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&posAddParam);

    paramCollector.add(&brightnessParam);
    countParam.set("/leds/count", 0, NUM_LEDS, 150);
    countParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&countParam);

    // uint32_t ha = CRGB(255,0,0);

    // artnetInit(&paramCollector);
    printer.begin();
    setupEsparam(&printer);
    // wt32Params.wifiEnableParam.setBoolValue(0);
    startNetwork();
    artnetBegin();

    FastLED.addLeds<LED_TYPE, LED_DATA_1_PIN, COLOR_ORDER>(leds, 0, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_2_PIN, COLOR_ORDER>(leds, NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_3_PIN, COLOR_ORDER>(leds, 2 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_4_PIN, COLOR_ORDER>(leds, 3 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_5_PIN, COLOR_ORDER>(leds, 4 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_6_PIN, COLOR_ORDER>(leds, 5 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_7_PIN, COLOR_ORDER>(leds, 6 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_8_PIN, COLOR_ORDER>(leds, 7 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);

    shadelPrecalc();

    for(int i = 0; i < SHADER_STRIPS_COUNT; i++){
        int addr = addresses[i][0];
        strips[i].pg.bindPixels(
            pixels+addr,
            addresses[i][1]-addresses[i][0],
            addr,
            NUM_LEDS
        );
        strips[i].map(
            Vec2(0.0,0.0),
            Vec2(1.0,1.0)
        );
    }

    digitalWrite(STATUS_LED_PIN, LOW);

    dripRampA.go(.0);
    dripRampB.go(.0);
}

CRGB colorA = CRGB::Black;
CRGB colorB = CRGB::Black;
float inc = 0;

CRGB annexShader(Vec2 _pos, CRGB _c){
    float f = _pos.x;
    f = smoothstep(0.2, 0.8, f);
    // f *= f;
    return blend(colorA, colorB, f * 255);
}

CRGB breathingShader(Vec2 _pos, CRGB _c){
    float f = _pos.x * 12.3 + inc * .4;
    f = sinLook(f) * .5 + .5;
    f *= f;
    f = smoothstep(0.2, 1., f);
    return blend(CRGB::Black, _c, f * 255);
}

CRGB slowPulseShader(Vec2 _pos, CRGB _c){
    float f = sinLook(_pos.x * 3.523 + inc) * .5 + .5;
    f= smoothstep(0.4, 1.0, f);
    return blend(CRGB::Black, _c, f * 255);
}

CRGB floodShader(Vec2 _pos, CRGB _c){
    float f = sinLook(_pos.x * 37.14 + inc *.1) * .5 + .5;
    return CHSV(f * 255, 255, 255);
}

CRGB squareShader(Vec2 _pos, CRGB _c){
    // float f = (_pos.x+_pos.y)*4.+inc;
    // f = sinLook(f)*.5+.5;
    // f = smoothstep(0.2, 1., f);
    // f*=f*f;
    // // f= 1.0-f;
    // return blend(CRGB::Black, CRGB(255,10,200), f*255);
    _pos *= -1.;
    _pos *= 666.;
    _pos += inc * 50.;
    float f = inoise8(_pos.x, _pos.y) / 255.;
    float g = inoise8(_pos.x / 3.3, _pos.y) / 255.;

    // f = sinLook(f)*.5+.5;
    f = smoothstep(0.1, 1., f);
    f *= f * f;
    // f= 1.0-f;
    return blend(CRGB::Black, CHSV(g*255, 255, 255), f * 255);

    // return blend(CRGB(100), _c, f*255);
}

CRGB sparkleShader(Vec2 _pos, CRGB _c){
    if(random(1000) < 10){
        return CRGB::White;
    }
    else {
        return _c;
    }
}

CRGB annexSignShader(Vec2 _pos, CRGB _c){
    float f = sinLook(_pos.x * 8 + inc * .05) * .5 + .5;
    f = smoothstep(0.4, 1.0, f);
    float h = mix(125, 175, f);
    return CHSV(h, 255, 255);
}

CRGB controllableShader(Vec2 _pos, CRGB _c){
    float f = sinLook(_pos.x * 3.523 + inc / 10) * .5 + .5;
    f = smoothstep(0.4, 1.0, f);
    float h = mix(hue1Param.v, hue2Param.v, f);
    return CHSV(h, 255, 255);
}

CRGB spiralShader(Vec2 _pos, CRGB _c){
    float f = sinLook(_pos.x * 3.1415 + inc) * .5 + .5;
    f= smoothstep(0.4, 1.0, f);
    return blend(CRGB::Black, _c, f * 255);
}

float dripVal;
float dripValA;
float dripValB;

CRGB dripShader(Vec2 _pos, CRGB _c){
    float f = sinLook(1.0 - _pos.x + dripVal);
    f = smoothstep(0.996, 1.0, f);
    return blend(CRGB::Black, _c, f * 255);
}

int dripStampA = 0;
int dripDelayA = 0;
int dripStampB = 0;
int dripDelayB = 0;

void loop() {
    digitalWrite(STATUS_LED_PIN, millis() % 250 < 100);
    updateEsparam();
    //artnetUpdate(leds, NUM_LEDS);
    printer.update();

    inc += 0.01;

    if(dripRampA.isDone() && dripStampA + dripDelayA < millis()){
        int haha = 777 + random(420);
        dripRampA.go(0., 1.0, haha);
        dripStampA = millis();
        dripDelayA = haha + random(4000) + 3000;
    }
    dripValA = dripRampA.get();
    dripValA = pow(dripValA, 4);

    if(dripRampB.isDone() && dripStampB + dripDelayB < millis()){
        int haha = 1120 + random(605);
        dripRampB.go(0., 1.0, haha);
        dripStampB = millis();
        dripDelayB = haha + random(3000) + 2000;
    }
    dripValB = dripRampB.get();
    dripValB = pow(dripValB, 4);

    for (int i = 0; i < 4; i++) { // chands
        float f = sinLook(i + inc * .01) * .5 + .5;
        strips[i].pg.setColor(CHSV(f * 255, 255, 255));
        strips[i].pg.applyShader(breathingShader);
    }
    for (int i = 0 ; i < TARP_EDGE_COUNT; i++) {
        strips[i + TARP_EDGE].pg.setColor(CRGB(255, 0, 255));
        strips[i + TARP_EDGE].pg.applyShader(squareShader);
    }
    dripVal = dripValA;
    strips[DRIP_A].pg.setColor(CRGB(100, 30, 255));
    strips[DRIP_A].pg.applyShader(dripShader);
    dripVal = dripValB;
    strips[DRIP_B].pg.setColor(CRGB(100, 30, 255));
    strips[DRIP_B].pg.applyShader(dripShader);
    for(int i = 0; i < 3; i++) { // vases
        strips[i + VASE_A].pg.setColor(CHSV(i * 50 + inc * 10., 255, 255));
        strips[i + VASE_A].pg.applyShader(slowPulseShader);
        strips[i + VASE_A].pg.applyShader(sparkleShader);
    }

    for(int i = 0; i < 2; i++) { // floods
        strips[i + FLOODS_A].pg.setColor(CRGB(0, 200, 0));
        strips[i + FLOODS_A].pg.applyShader(floodShader);
    }
    strips[FLOODS_C].pg.setColor(
        blend(
            CRGB(255, 0, 50),
            CRGB(50, 0, 255),
            (sinLook(inc * .1) * .5 + .5) * 255
        )
    );

    strips[ANNEX_SIGN].pg.setColor(CHSV(50 + inc * 10., 255, 255));
    strips[ANNEX_SIGN].pg.applyShader(annexSignShader);
    strips[ANNEX_SIGN].pg.applyShader(sparkleShader);

    float f = sinLook(1 + inc * .01) * .5 + .5;
    strips[PANEL_A].pg.setColor(CHSV(f * 255, 255, 255));
    strips[PANEL_A].pg.applyShader(breathingShader);

    strips[PANEL_B].pg.setColor(CHSV(f * 255, 128, 128));
    strips[PANEL_B].pg.applyShader(slowPulseShader);

    for(int i = 0 ; i <= 4 ; i++) {
        PixelGroup pg = strips[i + TREE_A].pg;
        pg.setColor(CHSV(80, 255, 255));
        // TODO: fade between hues, ~50 extent

        int pos = inc * 6 + .5;
        // TODO LOOP!
        int step1 = pos % pg.size;
        int step2 = (pos + 1) % pg.size;
        int step3 = (pos + 2) % pg.size;
        int step4 = (pos + 3) % pg.size;
        int step5 = (pos + 4) % pg.size;
        pg.pixels[step1].c = CHSV(80, 255, 100);
        pg.pixels[step2].c = CHSV(80, 255, 50);
        pg.pixels[step3].c = CRGB::Black;
        pg.pixels[step4].c = CHSV(80, 255, 50);
        pg.pixels[step5].c = CHSV(80, 255, 100);
        // TODO: upward spiral?
    }
    for(int s = TREE_C ; s <= TREE_E ; s++) {
        strips[s].pg.applyShader(spiralShader);
        strips[s].pg.applyShader(sparkleShader);
    }

    for(int i = 0; i < SHADER_STRIPS_COUNT; i++) {
        strips[i].pg.applyToLeds(leds);
    }


    if (! enableAnimParam.v) {
        for (int i = 0; i < NUM_LEDS; i++){
          leds[i] = (i == countParam.v) ? CRGB::Yellow : CRGB::Black;
        }
    }

    FastLED.show();
}

void updateBrightness(int v){
    FastLED.setBrightness(v);
}