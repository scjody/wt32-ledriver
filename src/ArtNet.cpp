#include "ArtNet.h"

namespace {
    WiFiUDP udpSocket;
    // AsyncUDP asyncUdpSocket;

    BoolParam enableArtnetParam;
    IntParam startUniverseParam;
    TextParam nodeNameParam;

    IPAddress ipAddress;
    byte packetBuffer[ARTNET_BUFFER_SIZE];

    //total universe count
    uint16_t universeCount;

    uint16_t currentSequence;

    bool doShow;
    artnet_reply_s artPollReply;
    void replyArtnetPoll();
    unsigned long artSyncTimeout;
    // CRGB artnetBuffer[NUM_LEDS];
    char nodeName[32];
    // artnet_reply_s ArtPollReply;
    // reply to art polL message
    void replyArtnetPoll() {
        artPollReply.ip[0] = ipAddress[0];
        artPollReply.ip[1] = ipAddress[1];
        artPollReply.ip[2] = ipAddress[2];
        artPollReply.ip[3] = ipAddress[3];
        // sprintf((char *)id, "Art-Net\0");
        memcpy(artPollReply.id, ART_NET_ID, sizeof(artPollReply.id));
        // memcpy(artPollReply.ip, local_ip, sizeof(artPollReply.ip));

        artPollReply.opCode = ART_POLL_REPLY;
        artPollReply.port =  ARTNET_PORT;

        memset(artPollReply.goodinput,  0x08, 4);
        memset(artPollReply.goodoutput,  0x80, 4);
        memset(artPollReply.porttypes,  0xc0, 4);

        uint8_t shortname [18];
        uint8_t longname [64];

        sprintf((char *)shortname, "wt32-_%s\0", nodeNameParam.v);
        sprintf((char *)longname, "wt32-eth01 based LED controller v:%.3f\0", 0.01);
        memcpy(artPollReply.shortname, shortname, sizeof(shortname));
        memcpy(artPollReply.longname, longname, sizeof(longname));

        artPollReply.etsaman[0] = 0;
        artPollReply.etsaman[1] = 0;
        artPollReply.verH       = 1;
        artPollReply.ver        = 0;
        artPollReply.subH       = 0;
        artPollReply.sub        = 0;
        artPollReply.oemH       = 0;
        artPollReply.oem        = 0xFF;
        artPollReply.ubea       = 0;
        artPollReply.status     = 0xd2;
        artPollReply.swvideo    = 0;
        artPollReply.swmacro    = 0;
        artPollReply.swremote   = 0;
        artPollReply.style      = 0;

        // maybe this should be reviewed
        artPollReply.numbportsH = 0;
        artPollReply.numbports  = universeCount; // can be set by LED_count.
        artPollReply.status2    = 0x08;

        artPollReply.bindip[0] = ipAddress[0];
        artPollReply.bindip[1] = ipAddress[1];
        artPollReply.bindip[2] = ipAddress[2];
        artPollReply.bindip[3] = ipAddress[3];

        uint8_t swin[4]  = {0x01,0x02,0x03,0x04};
        uint8_t swout[4] = {0x01,0x02,0x03,0x04};
        for(uint8_t i = 0; i < 4; i++)
        {
            artPollReply.swout[i] = swout[i];
            artPollReply.swin[i] = swin[i];
        }
        sprintf((char *)artPollReply.nodereport, "%i DMX output universes active.\0", artPollReply.numbports);
        udpSocket.beginPacket(udpSocket.remoteIP(), ARTNET_PORT);//send the packet to the broadcast address
        udpSocket.write((uint8_t *)&artPollReply, sizeof(artPollReply));
        udpSocket.endPacket();
    }
}

void artnetSetNodeName(const char * _str){
    strcpy(nodeName, _str);
}

// a first init, mostly to deal with parameters
void artnetInit(ParamCollector * _collector){
    enableArtnetParam.set("/artnet/enable", 1);
    enableArtnetParam.saveType = SAVE_ON_REQUEST;
    startUniverseParam.set("/artnet/universe", 1, 255, 1);
    startUniverseParam.saveType = SAVE_ON_REQUEST;
    startUniverseParam.inputType = NUMBER;
    nodeNameParam.set("/artnet/node_name", "alfredo");
    nodeNameParam.saveType = SAVE_ON_REQUEST;

    _collector->add(&enableArtnetParam);
    _collector->add(&startUniverseParam);
    _collector->add(&nodeNameParam);

}

// the real begin function that should be called once the network and stuff is started
void artnetBegin(){
    udpSocket.begin(ARTNET_PORT);
    artSyncTimeout = 0;
}

// exposed loop it
void artnetUpdate(CRGB * _leds, size_t _count){
    if(enableArtnetParam.v){
        int packetSize = udpSocket.parsePacket();
        if (packetSize <= ARTNET_BUFFER_SIZE && packetSize > 0) {
            // dunno if we should clear the buffer first
            udpSocket.read(packetBuffer, packetSize);
            // quick check for header
            if(memcmp(packetBuffer, ART_NET_ID, sizeof(ART_NET_ID)) != 0){
                return;
            }
            uint16_t opcode = packetBuffer[8] | packetBuffer[9] << 8;
            // oled.println(opcode, HEX);
            switch(opcode) {
                case ART_POLL:
                    replyArtnetPoll();
                    break;
                case ART_DMX:
                    {
                        uint16_t seq = packetBuffer[12];
                        uint16_t uni = packetBuffer[14] | packetBuffer[15] << 8;
                        uint16_t len = packetBuffer[17] | packetBuffer[16] << 8;
                        // Serial.printf("uni:%02i len:%03i seq:%03i\n",uni,len,seq);
                        universeCount = 1;
                        if(uni >= startUniverseParam.v-1){
                            if(uni < startUniverseParam.v-1 + universeCount){
                                uni -= startUniverseParam.v-1;
                                memcpy(_leds + uni * (512/3), packetBuffer + ART_DMX_START, len);
                                // Serial.println(_leds[0].r);
                    // #ifdef RGBW_MODE
                    //             // uni is not devided by 4 due to drawingMemory vs leds
                    //             memcpy(drawingMemory+uni*512, _data, _len);
                    // #else
                                // memcpy(artnetBuffer+ uni * (512/3), _data, _len);
                    // #endif
                            }
                        }
                        // if we dont receive artsync packet for 4 seconds
                        // we switch to non-Synchronous mode
                        if(artSyncTimeout+4000 < millis()){
                            if(seq != currentSequence){
                                currentSequence = seq;
                                FastLED.show();
                            }
                        }       
                    }
                    break;
                case ART_SYNC:
                    artSyncTimeout = millis();
                    FastLED.show();
                    break;
            }
        }
    }
}
